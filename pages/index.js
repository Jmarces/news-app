import Head from 'next/head';
import { Toolbar } from '../components/toolbar';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <>
      <Head>
        <title>News App</title>
      </Head>

      <div className="page-container">
        <Toolbar />
        <div className={styles.main}>
          <h1>News App made with NextJS and APIs</h1>

          <h3>Latest and Top news in your area!</h3>
        </div>
      </div>
    </>
  );
}